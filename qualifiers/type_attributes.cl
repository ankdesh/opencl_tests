struct attribute { short f[3]; } __attribute__ ((aligned));
struct attribute_n { short f[3]; } __attribute__ ((aligned (8)));
struct __attribute__ ((packed)) my_packed_struct
    {
        float a;
        struct attribute_n s;
    };
__kernel void pseudo_kernel(struct my_packed_struct *a,float* b)
{
  (*a).a += *b;
}
__kernel void pseudo_kernel2(struct attribute *a,float* b)
{
  (*a).f[0] += *b;
}
