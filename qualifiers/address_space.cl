__kernel void add__global(__global float* a)
{
  (*a)++;
}

__kernel void add_global(global float* a)
{
  (*a)++;
}
__kernel void add__local(__local float* a)
{
  (*a)++;
}

__kernel void add_local(local float* a)
{
  (*a)++;
}
__kernel void add__constant(__constant float* a)
{
  (*a)++;
}

__kernel void add_constant(constant float* a)
{
  (*a)++;
}
__kernel void add__private(__private float* a)
{
  __private float* b = a;
  (*b)++;
}

__kernel void add_private(private float* a)
{
  (*a)++;
}
