__kernel void add_vec_type_hint(float *a, float *b) //  __attribute__((vec_type_hint(float4)))
{
  (*a) += (*b);
}

__kernel void add_work_group_size_hint(float *a, float *b) //  __attribute__((work_group_size_hint(1,2,3)))
{
  (*a) += (*b);
}

__kernel void add_reqd_work_group_size(float *a, float *b) //  __attribute__((reqd_work_group_size(1,2,3)))
{
  (*a) += (*b);
}

__kernel void add_err(float *a, float *b) //  __attribute__((should_be_an_error_hint(float4)))
{
  (*a) += (*b);
}

