__kernel void add_char3(char3* a,char3* b)
{
 (*a).x += (*b).x;
}

__kernel void add_char4(char4* a,char4* b)
{
 (*a).x += (*b).x;
}

__kernel void add_char16_1(char16* a,char16* b)
{
 (*a).x += (*b).x;
}

__kernel void add_char16_2(char16* a,char16* b)
{
 (*a).s7 += (*b).s7;
}

__kernel void add_char16_3(char16* a,char16* b)
{
 (*a).sA += (*b).sA;
}

__kernel void add_uchar3(uchar3* a,uchar3* b)
{
 (*a).x += (*b).x;
}

__kernel void add_uchar4(uchar4* a,uchar4* b)
{
 (*a).x += (*b).x;
}

__kernel void add_uchar16_1(uchar16* a,uchar16* b)
{
 (*a).x += (*b).x;
}

__kernel void add_uchar16_2(uchar16* a,uchar16* b)
{
 (*a).s7 += (*b).s7;
}

__kernel void add_uchar16_3(uchar16* a,uchar16* b)
{
 (*a).sA += (*b).sA;
}

__kernel void add_short3(short3* a,short3* b)
{
 (*a).x += (*b).x;
}

__kernel void add_short4(short4* a,short4* b)
{
 (*a).x += (*b).x;
}

__kernel void add_short16_1(short16* a,short16* b)
{
 (*a).x += (*b).x;
}

__kernel void add_short16_2(short16* a,short16* b)
{
 (*a).s7 += (*b).s7;
}

__kernel void add_short16_3(short16* a,short16* b)
{
 (*a).sA += (*b).sA;
}

__kernel void add_ushort3(ushort3* a,ushort3* b)
{
 (*a).x += (*b).x;
}

__kernel void add_ushort4(ushort4* a,ushort4* b)
{
 (*a).x += (*b).x;
}

__kernel void add_ushort16_1(ushort16* a,ushort16* b)
{
 (*a).x += (*b).x;
}

__kernel void add_ushort16_2(ushort16* a,ushort16* b)
{
 (*a).s7 += (*b).s7;
}

__kernel void add_ushort16_3(ushort16* a,ushort16* b)
{
 (*a).sA += (*b).sA;
}

__kernel void add_int3(int3* a,int3* b)
{
 (*a).x += (*b).x;
}

__kernel void add_int4(int4* a,int4* b)
{
 (*a).x += (*b).x;
}

__kernel void add_int16_1(int16* a,int16* b)
{
 (*a).x += (*b).x;
}

__kernel void add_int16_2(int16* a,int16* b)
{
 (*a).s7 += (*b).s7;
}

__kernel void add_int16_3(int16* a,int16* b)
{
 (*a).sA += (*b).sA;
}

__kernel void add_uint3(uint3* a,uint3* b)
{
 (*a).x += (*b).x;
}

__kernel void add_uint4(uint4* a,uint4* b)
{
 (*a).x += (*b).x;
}

__kernel void add_uint16_1(uint16* a,uint16* b)
{
 (*a).x += (*b).x;
}

__kernel void add_uint16_2(uint16* a,uint16* b)
{
 (*a).s7 += (*b).s7;
}

__kernel void add_uint16_3(uint16* a,uint16* b)
{
 (*a).sA += (*b).sA;
}

__kernel void add_long3(long3* a,long3* b)
{
 (*a).x += (*b).x;
}

__kernel void add_long4(long4* a,long4* b)
{
 (*a).x += (*b).x;
}

__kernel void add_long16_1(long16* a,long16* b)
{
 (*a).x += (*b).x;
}

__kernel void add_long16_2(long16* a,long16* b)
{
 (*a).s7 += (*b).s7;
}

__kernel void add_long16_3(long16* a,long16* b)
{
 (*a).sA += (*b).sA;
}

__kernel void add_ulong3(ulong3* a,ulong3* b)
{
 (*a).x += (*b).x;
}

__kernel void add_float3(float3* a,float3* b)
{
 (*a).x += (*b).x;
}

__kernel void add_float4(float4* a,float4* b)
{
 (*a).x += (*b).x;
}

__kernel void add_float16_1(float16* a,float16* b)
{
 (*a).x += (*b).x;
}

__kernel void add_float16_2(float16* a,float16* b)
{
 (*a).s7 += (*b).s7;
}

__kernel void add_float16_3(float16* a,float16* b)
{
 (*a).sA += (*b).sA;
}

