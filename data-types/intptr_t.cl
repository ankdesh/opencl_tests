typedef int intptr_t;
//typedef __typeof( (int*) 0) intptr_t;
typedef uint uintptr_t;

__kernel void intptr_t_kernel(int *a, int *b)
{
  intptr_t c = (intptr_t)a;
  b = (int*) c;
  *a += *b;
}
