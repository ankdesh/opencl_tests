__kernel void bool_kernel(bool *a, bool b)
{
  *a &= b;
}


__kernel void uchar_kernel(uchar *a, uchar b)
{
  *a += b;
}

__kernel void short_kernel(short *a, short b)
{
  *a += b;
}

__kernel void uint_kernel(uint *a, uint b)
{
  *a += b;
}

__kernel void long_kernel(long *a, long b)
{
  *a += b;
}

__kernel void float_kernel(float *a, float b)
{
  *a += b;
}

__kernel void half_kernel(half *a, float b)
{
  *a += b;
}


__kernel void size_t_kernel(float *a)
{
  size_t b = *a;
  *a += b;
}

__kernel void ptrdiff_t_kernel(float *a, float *b)
{
  ptrdiff_t c = a - b;
  *a += c;
}


