#pragma OPENCL EXTENSION cl_khr_fp64 : enable

__kernel void add_double3(double3* a,double3* b)
{
 (*a)+=(*b);
}

__kernel void add_double4(double4* a,double4* b)
{
 (*a)+=(*b);
}

__kernel void add_double16(double16* a,double16* b)
{
 (*a)+=(*b);
}

