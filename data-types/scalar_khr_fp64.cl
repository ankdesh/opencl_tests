#pragma OPENCL EXTENSION cl_khr_fp64 : enable

__kernel void double_kernel(double *a, double b)
{
  *a += b;
}
