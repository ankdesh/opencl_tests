__kernel void add_char(char* a,char* b)
{
 (*a)+=(*b);
}

__kernel void add_char3(char3* a,char3* b)
{
 (*a)+=(*b);
}

__kernel void add_char4(char4* a,char4* b)
{
 (*a)+=(*b);
}

__kernel void add_char16(char16* a,char16* b)
{
 (*a)+=(*b);
}

__kernel void add_uchar(uchar* a,uchar* b)
{
 (*a)+=(*b);
}

__kernel void add_uchar3(uchar3* a,uchar3* b)
{
 (*a)+=(*b);
}

__kernel void add_uchar4(uchar4* a,uchar4* b)
{
 (*a)+=(*b);
}

__kernel void add_uchar16(uchar16* a,uchar16* b)
{
 (*a)+=(*b);
}

__kernel void add_short(short* a,short* b)
{
 (*a)+=(*b);
}

__kernel void add_short3(short3* a,short3* b)
{
 (*a)+=(*b);
}

__kernel void add_short4(short4* a,short4* b)
{
 (*a)+=(*b);
}

__kernel void add_short16(short16* a,short16* b)
{
 (*a)+=(*b);
}

__kernel void add_ushort(ushort* a,ushort* b)
{
 (*a)+=(*b);
}

__kernel void add_ushort3(ushort3* a,ushort3* b)
{
 (*a)+=(*b);
}

__kernel void add_ushort4(ushort4* a,ushort4* b)
{
 (*a)+=(*b);
}

__kernel void add_ushort16(ushort16* a,ushort16* b)
{
 (*a)+=(*b);
}

__kernel void add_int(int* a,int* b)
{
 (*a)+=(*b);
}

__kernel void add_int3(int3* a,int3* b)
{
 (*a)+=(*b);
}

__kernel void add_int4(int4* a,int4* b)
{
 (*a)+=(*b);
}

__kernel void add_int16(int16* a,int16* b)
{
 (*a)+=(*b);
}

__kernel void add_uint(uint* a,uint* b)
{
 (*a)+=(*b);
}

__kernel void add_uint3(uint3* a,uint3* b)
{
 (*a)+=(*b);
}

__kernel void add_uint4(uint4* a,uint4* b)
{
 (*a)+=(*b);
}

__kernel void add_uint16(uint16* a,uint16* b)
{
 (*a)+=(*b);
}

__kernel void add_long(long* a,long* b)
{
 (*a)+=(*b);
}

__kernel void add_long3(long3* a,long3* b)
{
 (*a)+=(*b);
}

__kernel void add_long4(long4* a,long4* b)
{
 (*a)+=(*b);
}

__kernel void add_long16(long16* a,long16* b)
{
 (*a)+=(*b);
}

__kernel void add_ulong(ulong* a,ulong* b)
{
 (*a)+=(*b);
}

__kernel void add_ulong3(ulong3* a,ulong3* b)
{
 (*a)+=(*b);
}

__kernel void add_ulong4(ulong4* a,ulong4* b)
{
 (*a)+=(*b);
}

__kernel void add_ulong16(ulong16* a,ulong16* b)
{
 (*a)+=(*b);
}

__kernel void add_float(float* a,float* b)
{
 (*a)+=(*b);
}

__kernel void add_float3(float3* a,float3* b)
{
 (*a)+=(*b);
}

__kernel void add_float4(float4* a,float4* b)
{
 (*a)+=(*b);
}

__kernel void add_float16(float16* a,float16* b)
{
 (*a)+=(*b);
}

