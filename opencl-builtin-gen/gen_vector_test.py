'''Enter the list of basic data types which have here  '''
basicDataTypesVec = [ "char" ,"uchar" ,"short", "ushort", "int", "uint", "long", "ulong", "float"]
basicDataTypesDirect = []

def gen_tests(listOfdataTypes = [], fileName = ""):
  filePtr = open(fileName,"w")
  for typeName in listOfdataTypes:
    kernel_func = "__kernel void add_{0}({1}* a,{1}* b)\n{2}\n (*a)+=(*b);\n{3}\n\n".format(typeName.replace(" ","_"),typeName,"{","}") 
    filePtr.write(kernel_func)
  filePtr.close()

def createFuncList():
  listOfFuncs = []
  for eachDT in basicDataTypesVec:
    ''' Scalar version '''
    listOfFuncs.append(eachDT) 
    ''' Vector version '''
    for i in [3,4,16]:
      listOfFuncs.append(eachDT+str(i))
  '''nxm datatypes'''
  for eachDT in basicDataTypesDirect:
    listOfFuncs.append(eachDT)
  return listOfFuncs

if __name__ == "__main__":
  funcList = createFuncList()
  print funcList
  gen_tests(funcList,"vector.cl")
