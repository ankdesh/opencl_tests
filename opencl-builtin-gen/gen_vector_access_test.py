'''Enter the list of basic data types which have here  '''
basicDataTypesVec = [ "char" ,"uchar" ,"short", "ushort", "int", "uint", "long", "ulong", "float"]
basicDataTypesDirect = []

def gen_tests(listOfdataTypes = [], fileName = ""):
  filePtr = open(fileName,"w")
  for typeName in listOfdataTypes:
    for i in [1,4,15]:
      kernel_body = gen_str_func_body(i)
      kernel_func = "__kernel void add_{0}({1}* a,{1}* b)\n{2}\n {3};\n{4}\n\n".format(typeName.replace(" ","_"),typeName,"{",kernel_body,"}") 
      filePtr.write(kernel_func)
  filePtr.close()

def gen_str_func_body(vecLenMax = 0):
  accessNo = ""
  if vecLenMax < 2:
    accessNo = ".x"
  if (vecLenMax >=2) and (vecLenMax < 4):
    accessNo = ".s3"
  if vecLenMax >=4 and vecLenMax < 8:
    accessNo = ".s7"
  if vecLenMax >=8 and vecLenMax < 16:
    accessNo = ".sA"
  resultString = "(*a){0} += (*b){0}".format(accessNo)
  return resultString
  

def createFuncList():
  listOfFuncs = []
  for eachDT in basicDataTypesVec:
    ''' Scalar version '''
    listOfFuncs.append(eachDT) 
    ''' Vector version '''
    for i in [3,4,16]:
      listOfFuncs.append(eachDT+str(i))
  '''nxm datatypes'''
  for eachDT in basicDataTypesDirect:
    listOfFuncs.append(eachDT)
  return listOfFuncs

if __name__ == "__main__":
  funcList = createFuncList()
  gen_tests(funcList,"vector_access.cl")

