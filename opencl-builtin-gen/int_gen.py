def genVecFuncDeclrHomo(funcNames = "" , typeName = "" ,vecs = [1,2,3,4,8,16],  numArgs = 1):
  ''' Generates - typeName(n) funcNames(typeName(n)...numArgs times); '''
  result = []
  funcDeclr = ""
  for i in vecs:
    if i == 1:
      funcDeclr = typeName + " " + funcNames + "("
      for j in range(numArgs):
        funcDeclr += typeName 
        if(j != numArgs-1 ):
          funcDeclr += ", "
    else:
      funcDeclr = typeName + str(i) + " " + funcNames + "("
      for j in range(numArgs):
        funcDeclr += typeName + str(i) 
        if(j != numArgs-1 ):
          funcDeclr += ", "
    funcDeclr += ") __attribute__((overloadable));"
    result.append(funcDeclr)
  return result

def genVecFuncDeclrHetero(funcNames = "" , returnType ="",  argTypeNames = [] ,vecs = [1,2,3,4,8,16], maskArg = []):
  ''' Generates - returnType funcNames(argTypeName([0], argTypeName[1]); Type name gives the type for 
         return value and arguments. MaskArg determines if the arg will be scalar or vector
          If maskArg == 0, type is only scalar. Else it is vector of vecs length '''
  result = []
  funcDeclr = ""
  for i in vecs:
    if i == 1:
      funcDeclr = returnType + " " + funcNames + "("
      for j,arg in enumerate(argTypeNames):
        funcDeclr += arg
        if(j != len(argTypeNames)-1 ):
          funcDeclr += ", "
    else:
      funcDeclr = returnType + str(i) + " " + funcNames + "("
      for j,arg in enumerate(argTypeNames):
        funcDeclr += arg 
        if(maskArg[j] == 1):
          funcDeclr += str(i)
        if(j != len(argTypeNames)-1 ):
          funcDeclr += ", "
    funcDeclr += ") __attribute__((overloadable));"
    result.append(funcDeclr)
  return result



if __name__ == "__main__":
#  tempDecr = vecFuncDeclr()
  result = genVecFuncDeclrHomo(funcNames = "sin", typeName = "float")
  print result
  result = genVecFuncDeclrHetero(funcNames = "sin", returnType = "float", argTypeNames = ["float","float"], maskArg = [0,1])
  print result
