// ---
// No proper definitions in current clc 
uint get_global_offset(int a){return 0;}
uint get_work_dim(){return 0;}
// ---

__kernel void test_get_global_id(int* a)
{
  *a = get_global_id(0);
}

__kernel void test_get_global_size(int* a)
{
  *a = get_global_size(0);
}

__kernel void test_get_local_id(int* a)
{
  *a = get_local_id(0);
}

__kernel void test_get_local_size(int* a)
{
  *a = get_local_size(0);
}

__kernel void test_get_group_id(int* a)
{
  *a = get_group_id(0);
}

__kernel void test_get_num_groups(int* a)
{
  *a = get_num_groups(0);
}

__kernel void test_get_global_offset(int* a)
{
  *a = get_global_offset(0);
}

__kernel void test_get_work_dim(int* a)
{
  *a = get_work_dim();
}
